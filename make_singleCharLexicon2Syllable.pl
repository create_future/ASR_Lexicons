use strict;
use warnings;
use Encode;
my $single_char_lexicon = $ARGV[0];
open L, "<$single_char_lexicon" or die "$!";
my @lexiconLines = <L>;
chomp @lexiconLines;
close L;
my %syllableDict;
my %charDict;
my $k=0;
while ($k < @lexiconLines) {
	 my $currline = $lexiconLines[$k];
	 my @lineUnits = split(/ /, $currline);
	 if(!exists($charDict{$lineUnits[0]}))
	 {
	 		$charDict{$lineUnits[0]} = 1;
	 }
	 for (my $i = 2; $i < @lineUnits; $i++) {
	 	 if(! exists($syllableDict{$lineUnits[$i]}))
	 	 {
	 	 		$syllableDict{$lineUnits[$i]} = 1;
	 	 }
	 }
	 $k++;
}

my @allChars = keys %charDict;
my @allSyllables = keys %syllableDict;
my @sorted_chars = sort @allChars;
my @sorted_syllables = sort @allSyllables;
$k=1;
foreach my $syllable (@sorted_syllables)
{
	$syllableDict{$syllable} = $k;
	$k++;
}

$k=0;
while ($k < @lexiconLines) {
	 my $currline = $lexiconLines[$k];
	 my @lineUnits = split(/ /, $currline);
	 shift @lineUnits;
	 for (my $i = 1; $i < @lineUnits; $i++) {
	 	 $lineUnits[$i] = $syllableDict{$lineUnits[$i]};
	 }
	 $currline = join(" ", @lineUnits);
	 print $currline."\n";
	 $k++;
}

##outut files 
open C, ">lexicon.txt" or die "$!";
my $i=1;
foreach my $onechar (@sorted_chars)
{
	print C $onechar." $i\n";
	$i++;
}
close C;
open S,">syllable.txt";
$i=1;
foreach my $onesyllable (@sorted_syllables)
{
	print S $onesyllable." $i\n";
	$i++;
}

close S;
